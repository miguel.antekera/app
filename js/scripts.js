// Transferencia
$(function() {

    var form = $('#ajax-contact');
    var formMessages = $('#form-messages');

    $(form).submit(function(e) {
        e.preventDefault();

        var formData = $(form).serialize();

        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        })

        .done(function(response) {
                $(formMessages).removeClass('alert alert-danger fade in');
                $(formMessages).addClass('alert alert-success fade in');

                $(formMessages).text(response);

                $('#name_trans').val('');
                $('#email_trans').val('');
                $('#transferencia').val('');
                $('#message_trans').val('');
            })
            .fail(function(data) {
                $(formMessages).removeClass('alert alert-success fade in');
                $(formMessages).addClass('alert alert-danger fade in');

                if (data.responseText !== '') {
                    $(formMessages).text(data.responseText);
                } else {
                    $(formMessages).text('Error, los datos no han sido enviados por favor intenta de nuevo.');
                }
            });

    });

});


$(document).ready(function() {

    $(".ccinfo").show();
    $(":radio[name=cctype]").click(function() {
        if ($(this).hasClass("isPayPal")) {
            $(".ccinfo").slideUp("fast");
        } else {
            $(".ccinfo").slideDown("fast");
        }
        resetCCHightlight();
    });

    $("input[name=ccn]").bind('paste', function(e) {
        var el = $(this);
        setTimeout(function() {
            var text = $(el).val();
            resetCCHightlight();
            checkNumHighlight(text);
        }, 100);
    });

    // change amount on click

    var amount1 = "0.10";
    var amount2 = "20.00";
    var amount3 = "50.00";
    var amount4 = "100.00";

    $(".amount1").on("click", function() {
        $("#amount").val(amount1);
        $("#item_description").val("Monto donado: $ "+ amount1 + "");
    });
    $(".amount2").on("click", function() {
        $("#amount").val(amount2);
        $("#item_description").val("Monto donado: $ "+ amount2 + "");
    });
    $(".amount3").on("click", function() {
        $("#amount").val(amount3);
        $("#item_description").val("Monto donado: $ "+ amount3 + "");
    });
    $(".amount4").on("click", function() {
        $("#amount").val(amount4);
        $("#item_description").val("Monto donado: $ "+ amount4 + "");
    });
    $(".amount5").on("click", function() {
        $("#amount").val("");
        $("#item_description").val("");
    }); 

    $( "#amount" ).keyup(function() {
      var amount5 = $(this).val();
      $("#item_description").val("Monto donado: $ "+ amount5 + "");
    });

});