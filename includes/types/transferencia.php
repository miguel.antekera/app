<p>  Los datos bancarios para transferir son: </p>
<!--googleoff: all-->
<p>
  <strong> Beneficiario: </strong> CITIZENS FOR INTEGRITY, INC <br />
  <strong> Dirección Beneficiario:</strong> 75 Valencia Ave. Suite 703, Coral Gable , Fl 33134. USA<br />
  <strong> Cuenta:</strong> 9138207756<br />
  <strong> Banco:</strong> Citibank N.A.<br />
  <strong> Dirección del banco:</strong> Key Biscayne, 84 Crandon Blvd, Key Biscayne, Fl 33149 USA<br />
  <strong> ABA: </strong> 266086554<br />
  <strong> SWIFT:</strong> CITIUS33<br />
</p>
<!-- googleon: all -->
<br />
<p>
  Una vez realizada la transferencia, por favor haga click  <a id="modal-719896" href="#modal-container-719896" role="button" data-toggle="modal">AQUÍ</a> para notificar su transferencia.
</p>
<p>
  Ud. recibirá otro correo donde se confirma la recepción efectiva de su donación. Por favor guarde ese mensaje para su registro.
</p>
<p>
  En caso de que tenga alguna pregunta sobre su donación o le gustaría recibir más información sobre nuestro trabajo,
  por favor no dude en consultarnos por <a href="mailto:antiguiso@transparencia.org.ve">antiguiso@transparencia.org.ve</a> o visite nuestra página web <a href="https://www.transparencia.org.ve" target="_blank">www.transparencia.org.ve</a>.
  También le invitamos a que participe en nuestras redes <a href="https://www.facebook.com/Transparenciav" target="_blank">Facebook</a> y <a href="https://twitter.com/nomasguiso" target="_blank">Twitter</a>.
</p>
<p>
  Atentamente,<br /> Equipo Transparencia Venezuela
</p>
<a id="modal-719896" href="#modal-container-719896" role="button" data-toggle="modal" class="btn btn-success btn-lg">Confirmar Transferencia</a>
<div class="modal fade" id="modal-container-719896" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        ×
        </button>
        <h4 class="modal-title" id="myModalLabel">
        Confirmar Transferencia Bancaria
        </h4>
      </div>
      <div class="modal-body">
        <p> Coloque los datos a continuación si ya realizó la transferencia bancaria. </p>
        
        <div id="form-messages"></div>
        <form id="ajax-contact" method="post" action="includes/types/mailer_transferencia.php">
          <div class="row">
            <div class="col-sm-6">

              <div class="form-group">
                <label for="transferencia">Número de Transferencia:</label>
                <input type="text" id="transferencia" name="transferencia" class="form-control" required>
              </div>

              <div class="form-group">
                <label for="name">Nombre:</label>
                <input type="text" id="name_trans" name="name_trans" class="form-control" required>
              </div>

              <div class="form-group">
                <label for="ciudad_transferencia">Ciudad:</label>
                <input type="text" id="ciudad_transferencia" name="ciudad_transferencia" class="form-control" required>
              </div>

              <div class="form-group">
                <label for="pais_transferencia">País:</label>
                <select name="pais_transferencia" id="pais_transferencia" class="form-control" onchange="checkFieldBack(this);">
                  <option value="">Selecciona</option>
                  <option value="VE">Venezuela</option>
                  <option value="US">Estados Unidos</option>
                  <option value="CA">Canada</option>
                  <option value="UK">Reino Unido</option>
                  <option value="AU">Australia</option>
                  <option value="AF">Afghanistan</option>
                  <option value="AL">Albania</option>
                  <option value="DZ">Algeria</option>
                  <option value="AS">American Samoa</option>
                  <option value="AD">Andorra</option>
                  <option value="AO">Angola</option>
                  <option value="AI">Anguilla</option>
                  <option value="AQ">Antarctica</option>
                  <option value="AG">Antigua and Barbuda</option>
                  <option value="AR">Argentina</option>
                  <option value="AM">Armenia</option>
                  <option value="AW">Aruba</option>
                  <option value="AT">Austria</option>
                  <option value="AZ">Azerbaijan</option>
                  <option value="BS">Bahamas</option>
                  <option value="BH">Bahrain</option>
                  <option value="BD">Bangladesh</option>
                  <option value="BB">Barbados</option>
                  <option value="BY">Belarus</option>
                  <option value="BE">Belgium</option>
                  <option value="BZ">Belize</option>
                  <option value="BJ">Benin</option>
                  <option value="BM">Bermuda</option>
                  <option value="BT">Bhutan</option>
                  <option value="BO">Bolivia</option>
                  <option value="BA">Bosnia and Herzegovina</option>
                  <option value="BW">Botswana</option>
                  <option value="BR">Brazil</option>
                  <option value="BN">Brunei Darussalam</option>
                  <option value="BG">Bulgaria</option>
                  <option value="BF">Burkina Faso</option>
                  <option value="BI">Burundi</option>
                  <option value="KH">Cambodia</option>
                  <option value="CM">Cameroon</option>
                  <option value="CV">Cape Verde</option>
                  <option value="KY">Cayman Islands</option>
                  <option value="CF">Central African Republic</option>
                  <option value="TD">Chad</option>
                  <option value="CL">Chile</option>
                  <option value="CN">China</option>
                  <option value="CX">Christmas Island</option>
                  <option value="CC">Cocos (Keeling) Islands</option>
                  <option value="CO">Colombia</option>
                  <option value="KM">Comoros</option>
                  <option value="CG">Congo</option>
                  <option value="CD">Congo, The Democratic Republic of the</option>
                  <option value="CK">Cook Islands</option>
                  <option value="CR">Costa Rica</option>
                  <option value="CI">Cote D`Ivoire</option>
                  <option value="HR">Croatia</option>
                  <option value="CY">Cyprus</option>
                  <option value="CZ">Czech Republic</option>
                  <option value="DK">Denmark</option>
                  <option value="DJ">Djibouti</option>
                  <option value="DM">Dominica</option>
                  <option value="DO">Dominican Republic</option>
                  <option value="EC">Ecuador</option>
                  <option value="EG">Egypt</option>
                  <option value="SV">El Salvador</option>
                  <option value="GQ">Equatorial Guinea</option>
                  <option value="ER">Eritrea</option>
                  <option value="EE">Estonia</option>
                  <option value="ET">Ethiopia</option>
                  <option value="FK">Falkland Islands (Malvinas)</option>
                  <option value="FO">Faroe Islands</option>
                  <option value="FJ">Fiji</option>
                  <option value="FI">Finland</option>
                  <option value="FR">France</option>
                  <option value="GF">French Guiana</option>
                  <option value="PF">French Polynesia</option>
                  <option value="GA">Gabon</option>
                  <option value="GM">Gambia</option>
                  <option value="GE">Georgia</option>
                  <option value="DE">Germany</option>
                  <option value="GH">Ghana</option>
                  <option value="GI">Gibraltar</option>
                  <option value="GR">Greece</option>
                  <option value="GL">Greenland</option>
                  <option value="GD">Grenada</option>
                  <option value="GP">Guadeloupe</option>
                  <option value="GU">Guam</option>
                  <option value="GT">Guatemala</option>
                  <option value="GN">Guinea</option>
                  <option value="GW">Guinea-Bissau</option>
                  <option value="GY">Guyana</option>
                  <option value="HT">Haiti</option>
                  <option value="HN">Honduras</option>
                  <option value="HK">Hong Kong</option>
                  <option value="HU">Hungary</option>
                  <option value="IS">Iceland</option>
                  <option value="IN">India</option>
                  <option value="ID">Indonesia</option>
                  <option value="IR">Iran (Islamic Republic Of)</option>
                  <option value="IQ">Iraq</option>
                  <option value="IE">Ireland</option>
                  <option value="IL">Israel</option>
                  <option value="IT">Italy</option>
                  <option value="JM">Jamaica</option>
                  <option value="JP">Japan</option>
                  <option value="JO">Jordan</option>
                  <option value="KZ">Kazakhstan</option>
                  <option value="KE">Kenya</option>
                  <option value="KI">Kiribati</option>
                  <option value="KP">Korea North</option>
                  <option value="KR">Korea South</option>
                  <option value="KW">Kuwait</option>
                  <option value="KG">Kyrgyzstan</option>
                  <option value="LA">Laos</option>
                  <option value="LV">Latvia</option>
                  <option value="LB">Lebanon</option>
                  <option value="LS">Lesotho</option>
                  <option value="LR">Liberia</option>
                  <option value="LI">Liechtenstein</option>
                  <option value="LT">Lithuania</option>
                  <option value="LU">Luxembourg</option>
                  <option value="MO">Macau</option>
                  <option value="MK">Macedonia</option>
                  <option value="MG">Madagascar</option>
                  <option value="MW">Malawi</option>
                  <option value="MY">Malaysia</option>
                  <option value="MV">Maldives</option>
                  <option value="ML">Mali</option>
                  <option value="MT">Malta</option>
                  <option value="MH">Marshall Islands</option>
                  <option value="MQ">Martinique</option>
                  <option value="MR">Mauritania</option>
                  <option value="MU">Mauritius</option>
                  <option value="MX">Mexico</option>
                  <option value="FM">Micronesia</option>
                  <option value="MD">Moldova</option>
                  <option value="MC">Monaco</option>
                  <option value="MN">Mongolia</option>
                  <option value="MS">Montserrat</option>
                  <option value="MA">Morocco</option>
                  <option value="MZ">Mozambique</option>
                  <option value="NA">Namibia</option>
                  <option value="NP">Nepal</option>
                  <option value="NL">Netherlands</option>
                  <option value="AN">Netherlands Antilles</option>
                  <option value="NC">New Caledonia</option>
                  <option value="NZ">New Zealand</option>
                  <option value="NI">Nicaragua</option>
                  <option value="NE">Niger</option>
                  <option value="NG">Nigeria</option>
                  <option value="NO">Norway</option>
                  <option value="OM">Oman</option>
                  <option value="PK">Pakistan</option>
                  <option value="PW">Palau</option>
                  <option value="PS">Palestine Autonomous</option>
                  <option value="PA">Panama</option>
                  <option value="PG">Papua New Guinea</option>
                  <option value="PY">Paraguay</option>
                  <option value="PE">Peru</option>
                  <option value="PH">Philippines</option>
                  <option value="PL">Poland</option>
                  <option value="PT">Portugal</option>
                  <option value="PR">Puerto Rico</option>
                  <option value="QA">Qatar</option>
                  <option value="RE">Reunion</option>
                  <option value="RO">Romania</option>
                  <option value="RU">Russian Federation</option>
                  <option value="RW">Rwanda</option>
                  <option value="VC">Saint Vincent and the Grenadines</option>
                  <option value="MP">Saipan</option>
                  <option value="SM">San Marino</option>
                  <option value="SA">Saudi Arabia</option>
                  <option value="SN">Senegal</option>
                  <option value="SC">Seychelles</option>
                  <option value="SL">Sierra Leone</option>
                  <option value="SG">Singapore</option>
                  <option value="SK">Slovak Republic</option>
                  <option value="SI">Slovenia</option>
                  <option value="SO">Somalia</option>
                  <option value="ZA">South Africa</option>
                  <option value="ES">Spain</option>
                  <option value="LK">Sri Lanka</option>
                  <option value="KN">St. Kitts/Nevis</option>
                  <option value="LC">St. Lucia</option>
                  <option value="SD">Sudan</option>
                  <option value="SR">Suriname</option>
                  <option value="SZ">Swaziland</option>
                  <option value="SE">Sweden</option>
                  <option value="CH">Switzerland</option>
                  <option value="SY">Syria</option>
                  <option value="TW">Taiwan</option>
                  <option value="TI">Tajikistan</option>
                  <option value="TZ">Tanzania</option>
                  <option value="TH">Thailand</option>
                  <option value="TG">Togo</option>
                  <option value="TK">Tokelau</option>
                  <option value="TO">Tonga</option>
                  <option value="TT">Trinidad and Tobago</option>
                  <option value="TN">Tunisia</option>
                  <option value="TR">Turkey</option>
                  <option value="TM">Turkmenistan</option>
                  <option value="TC">Turks and Caicos Islands</option>
                  <option value="TV">Tuvalu</option>
                  <option value="UG">Uganda</option>
                  <option value="UA">Ukraine</option>
                  <option value="AE">United Arab Emirates</option>
                  <option value="UY">Uruguay</option>
                  <option value="UZ">Uzbekistan</option>
                  <option value="VU">Vanuatu</option>
                  <option value="VN">Viet Nam</option>
                  <option value="VG">Virgin Islands (British)</option>
                  <option value="VI">Virgin Islands (U.S.)</option>
                  <option value="WF">Wallis and Futuna Islands</option>
                  <option value="YE">Yemen</option>
                  <option value="YU">Yugoslavia</option>
                  <option value="ZM">Zambia</option>
                  <option value="ZW">Zimbabwe</option>
                </select>
              </div>

              <div class="form-group">
                <label for="zip_transferencia">Código Postal:</label>
                <input type="text" id="zip_transferencia" name="zip_transferencia" class="form-control">
              </div>

             </div>
             <div class="col-sm-6">

              <div class="form-group">
                <label for="monto_transferencia">Monto:</label>
                <input type="text" id="monto_transferencia" name="monto_transferencia" class="form-control" required>
              </div>
            
              <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" id="email_trans" name="email_trans" class="form-control" required>
              </div>

              <div class="form-group">
                <label for="direccion_transferencia">Dirección:</label>
                <input type="text" id="direccion_transferencia" name="direccion_transferencia" class="form-control">
              </div>

             <div class="form-group">
               <label for="estado_transferencia">Estado:</label>
               <input type="text" id="estado_transferencia" name="estado_transferencia" class="form-control">
             </div>
    
              <div class="form-group">
                <label for="message">Mensaje:</label>
                <input type="text" id="message_trans" name="message_trans" class="form-control">
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                <div class="clearfix"></div>
              </div>

            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">
        Cerrar
        </button>
      </div>
    </div>
  </div>
</div>