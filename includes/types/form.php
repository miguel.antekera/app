<div class="form_container">
  <?php echo $mess; ?>
  <form id="ff1" name="ff1" method="post" action="" enctype="multipart/form-data" onsubmit="return checkForm();" class="pppt_form">
    <div id="accordion">
      <?php if($show_form){ ?>
      <!-- PAYMENT BLOCK -->
      <h4 class="current"></h4>  
         
      <h4 class="title-paymode"></h4>

      <div class="pane" style="display:block">
        <?php if($show_services || $payment_mode=="RECUR"){
                  echo "<div class=form-group> <label>Monto: </label> <div class='clearfix'></div>";
                  switch($payment_mode){
                  
                  case"ONETIME":
                    //IF services specified in config file - we show services.
                    echo "<select class='form-control has-services' name='service' id='service' class='form-control' onchange='checkFieldBack(this)'><option value=''>Selecciona</option>";
                    foreach($services as $k=>$v){
                      echo "<option class='option_services' value='".$k."' ".($service==$k?" ":"").">".$v[0]." ($".number_format($v[1],2).")"."</option>";
                    }
                  echo "</select> </div>";
                break;
                case"RECUR":
                // Recurring payment options

                echo " <div class='clearfix'>
                  <div class='col-md-6 p0'>
                    <div class='form-group'>
                      <div class='btn-group'> ";

                      echo "
                      <button class='btn btn-default amount1' type='button'>
                        $0.1
                      </button> 
                      <button class='btn btn-default amount2' type='button'>
                        $20
                      </button> 
                      <button class='btn btn-default amount3' type='button'>
                        $50
                      </button> 
                      <button class='btn btn-default amount4' type='button'>
                        $100
                      </button>
                      <button class='btn btn-default amount5' type='button'>
                        Otro Monto
                      </button>
                      ";

                     echo " </div> 
                     <input class='form-control' placeholder='Monto a donar' name='item_description' id='item_description' type='text' class='form-control'  value=' $service ' onkeyup='checkFieldBack(this);' style='display: no ne;'/>
                    </div>
                  </div>
                  <div class='col-md-3 p0'>
                    <div class='form-group' id='other_amount'>
                      <input class='form-control' placeholder='$' name='amount' id='amount' type='text' class='form-control' value=' $amount '  onkeyup='checkFieldBack(this);noAlpha(this);' onkeypress='noAlpha(this);'>
                    </div>
                  </div>
                </div> "; 

                echo " </div> ";

                  // IF services specified in config file - we show services.
                // echo "<div class='row'> <div class='col-md-6'>  ";
                // echo "<select class='form-control has-anual' name='service' id='service' class='form-control' onchange='checkFieldBack(this)'><option value=''>Selecciona</option>";  
                //   foreach($recur_services as $k=>$v){
                //     echo "<option value='".$k."' ".($service==$k?" ":"").">".$v[0]." ($".number_format($v[1],2).")"."</option>";
                //   }
                // echo "</select> </div> </div>  "; 


              break;
              }
    } else { ?>
    <div class="clearfix">
      <div class="col-md-6 p0">
        <div class="form-group">
          <label>Monto:</label>
          <div class="clearfix"></div>
          <div class="btn-group">
            <button class="btn btn-default amount1" type="button">
              $0.1
            </button> 
            <button class="btn btn-default amount2" type="button">
              $20
            </button> 
            <button class="btn btn-default amount3" type="button">
              $50
            </button> 
            <button class="btn btn-default amount4" type="button">
              $100
            </button>
            <button class="btn btn-default amount5" type="button">
              Otro Monto
            </button>
          </div>

          <input class="form-control" placeholder="Monto a donar" name="item_description" id="item_description" type="text" class="form-control"  value="<?php echo $item_description;?>" onkeyup="checkFieldBack(this);" style="display: none;"/>
        </div>
      </div>
      <div class="col-md-3 p0">
        <div class="form-group" id="other_amount">
          <label>&nbsp;</label>
          <input class="form-control" placeholder="$" name="amount" id="amount" type="text" class="form-control" value="<?php echo $amount;?>"  onkeyup="checkFieldBack(this);noAlpha(this);" onkeypress="noAlpha(this);">
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <!-- PAYMENT BLOCK -->
  
  <!-- .form-group -->
  
  
  <!-- BILLING BLOCK -->
  <h4>Información Personal</h4>
  <div class="pane">

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label>Nombre:</label>
          <input name="fname" id="fname" type="text" class="form-control"  value="<?php echo $fname;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Apellido:</label>
          <input name="lname" id="lname" type="text" class="form-control"  value="<?php echo $lname;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Email:</label>
          <input name="email" id="email" type="text" class="form-control"  value="<?php echo $email;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Dirección:</label>
          <input name="address" id="address" type="text" class="form-control"  value="<?php echo $address;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Ciudad:</label>
          <input name="city" id="city" type="text" class="form-control"  value="<?php echo $city;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label>País:</label>
          <select name="country" id="country" class="form-control" onchange="checkFieldBack(this);">
            <option value="">Selecciona</option>
            <option value="VE" <?php echo $country=="VE"?:""?>>Venezuela</option>
            <option value="US" <?php echo $country=="US"?:""?>>Estados Unidos</option>
            <option value="CA" <?php echo $country=="CA"?:""?>>Canada</option>
            <option value="UK" <?php echo $country=="UK"?:""?>>Reino Unido</option>
            <option value="AU" <?php echo $country=="AU"?:""?>>Australia</option>
            <option value="AF" <?php echo $country=="AF"?:""?>>Afghanistan</option>
            <option value="AL" <?php echo $country=="AL"?:""?>>Albania</option>
            <option value="DZ" <?php echo $country=="DZ"?:""?>>Algeria</option>
            <option value="AS" <?php echo $country=="AS"?:""?>>American Samoa</option>
            <option value="AD" <?php echo $country=="AD"?:""?>>Andorra</option>
            <option value="AO" <?php echo $country=="AO"?:""?>>Angola</option>
            <option value="AI" <?php echo $country=="AI"?:""?>>Anguilla</option>
            <option value="AQ" <?php echo $country=="AQ"?:""?>>Antarctica</option>
            <option value="AG" <?php echo $country=="AG"?:""?>>Antigua and Barbuda</option>
            <option value="AR" <?php echo $country=="AR"?:""?>>Argentina</option>
            <option value="AM" <?php echo $country=="AM"?:""?>>Armenia</option>
            <option value="AW" <?php echo $country=="AW"?:""?>>Aruba</option>
            <option value="AT" <?php echo $country=="AT"?:""?>>Austria</option>
            <option value="AZ" <?php echo $country=="AZ"?:""?>>Azerbaijan</option>
            <option value="BS" <?php echo $country=="BS"?:""?>>Bahamas</option>
            <option value="BH" <?php echo $country=="BH"?:""?>>Bahrain</option>
            <option value="BD" <?php echo $country=="BD"?:""?>>Bangladesh</option>
            <option value="BB" <?php echo $country=="BB"?:""?>>Barbados</option>
            <option value="BY" <?php echo $country=="BY"?:""?>>Belarus</option>
            <option value="BE" <?php echo $country=="BE"?:""?>>Belgium</option>
            <option value="BZ" <?php echo $country=="BZ"?:""?>>Belize</option>
            <option value="BJ" <?php echo $country=="BJ"?:""?>>Benin</option>
            <option value="BM" <?php echo $country=="BM"?:""?>>Bermuda</option>
            <option value="BT" <?php echo $country=="BT"?:""?>>Bhutan</option>
            <option value="BO" <?php echo $country=="BO"?:""?>>Bolivia</option>
            <option value="BA" <?php echo $country=="BA"?:""?>>Bosnia and Herzegovina</option>
            <option value="BW" <?php echo $country=="BW"?:""?>>Botswana</option>
            <option value="BR" <?php echo $country=="BR"?:""?>>Brazil</option>
            <option value="BN" <?php echo $country=="BN"?:""?>>Brunei Darussalam</option>
            <option value="BG" <?php echo $country=="BG"?:""?>>Bulgaria</option>
            <option value="BF" <?php echo $country=="BF"?:""?>>Burkina Faso</option>
            <option value="BI" <?php echo $country=="BI"?:""?>>Burundi</option>
            <option value="KH" <?php echo $country=="KH"?:""?>>Cambodia</option>
            <option value="CM" <?php echo $country=="CM"?:""?>>Cameroon</option>
            <option value="CV" <?php echo $country=="CV"?:""?>>Cape Verde</option>
            <option value="KY" <?php echo $country=="KY"?:""?>>Cayman Islands</option>
            <option value="CF" <?php echo $country=="CF"?:""?>>Central African Republic</option>
            <option value="TD" <?php echo $country=="TD"?:""?>>Chad</option>
            <option value="CL" <?php echo $country=="CL"?:""?>>Chile</option>
            <option value="CN" <?php echo $country=="CN"?:""?>>China</option>
            <option value="CX" <?php echo $country=="CX"?:""?>>Christmas Island</option>
            <option value="CC" <?php echo $country=="CC"?:""?>>Cocos (Keeling) Islands</option>
            <option value="CO" <?php echo $country=="CO"?:""?>>Colombia</option>
            <option value="KM" <?php echo $country=="KM"?:""?>>Comoros</option>
            <option value="CG" <?php echo $country=="CG"?:""?>>Congo</option>
            <option value="CD" <?php echo $country=="CD"?:""?>>Congo, The Democratic Republic of the</option>
            <option value="CK" <?php echo $country=="CK"?:""?>>Cook Islands</option>
            <option value="CR" <?php echo $country=="CR"?:""?>>Costa Rica</option>
            <option value="CI" <?php echo $country=="CI"?:""?>>Cote D`Ivoire</option>
            <option value="HR" <?php echo $country=="HR"?:""?>>Croatia</option>
            <option value="CY" <?php echo $country=="CY"?:""?>>Cyprus</option>
            <option value="CZ" <?php echo $country=="CZ"?:""?>>Czech Republic</option>
            <option value="DK" <?php echo $country=="DK"?:""?>>Denmark</option>
            <option value="DJ" <?php echo $country=="DJ"?:""?>>Djibouti</option>
            <option value="DM" <?php echo $country=="DM"?:""?>>Dominica</option>
            <option value="DO" <?php echo $country=="DO"?:""?>>Dominican Republic</option>
            <option value="EC" <?php echo $country=="EC"?:""?>>Ecuador</option>
            <option value="EG" <?php echo $country=="EG"?:""?>>Egypt</option>
            <option value="SV" <?php echo $country=="SV"?:""?>>El Salvador</option>
            <option value="GQ" <?php echo $country=="GQ"?:""?>>Equatorial Guinea</option>
            <option value="ER" <?php echo $country=="ER"?:""?>>Eritrea</option>
            <option value="EE" <?php echo $country=="EE"?:""?>>Estonia</option>
            <option value="ET" <?php echo $country=="ET"?:""?>>Ethiopia</option>
            <option value="FK" <?php echo $country=="FK"?:""?>>Falkland Islands (Malvinas)</option>
            <option value="FO" <?php echo $country=="FO"?:""?>>Faroe Islands</option>
            <option value="FJ" <?php echo $country=="FJ"?:""?>>Fiji</option>
            <option value="FI" <?php echo $country=="FI"?:""?>>Finland</option>
            <option value="FR" <?php echo $country=="FR"?:""?>>France</option>
            <option value="GF" <?php echo $country=="GF"?:""?>>French Guiana</option>
            <option value="PF" <?php echo $country=="PF"?:""?>>French Polynesia</option>
            <option value="GA" <?php echo $country=="GA"?:""?>>Gabon</option>
            <option value="GM" <?php echo $country=="GM"?:""?>>Gambia</option>
            <option value="GE" <?php echo $country=="GE"?:""?>>Georgia</option>
            <option value="DE" <?php echo $country=="DE"?:""?>>Germany</option>
            <option value="GH" <?php echo $country=="GH"?:""?>>Ghana</option>
            <option value="GI" <?php echo $country=="GI"?:""?>>Gibraltar</option>
            <option value="GR" <?php echo $country=="GR"?:""?>>Greece</option>
            <option value="GL" <?php echo $country=="GL"?:""?>>Greenland</option>
            <option value="GD" <?php echo $country=="GD"?:""?>>Grenada</option>
            <option value="GP" <?php echo $country=="GP"?:""?>>Guadeloupe</option>
            <option value="GU" <?php echo $country=="GU"?:""?>>Guam</option>
            <option value="GT" <?php echo $country=="GT"?:""?>>Guatemala</option>
            <option value="GN" <?php echo $country=="GN"?:""?>>Guinea</option>
            <option value="GW" <?php echo $country=="GW"?:""?>>Guinea-Bissau</option>
            <option value="GY" <?php echo $country=="GY"?:""?>>Guyana</option>
            <option value="HT" <?php echo $country=="HT"?:""?>>Haiti</option>
            <option value="HN" <?php echo $country=="HN"?:""?>>Honduras</option>
            <option value="HK" <?php echo $country=="HK"?:""?>>Hong Kong</option>
            <option value="HU" <?php echo $country=="HU"?:""?>>Hungary</option>
            <option value="IS" <?php echo $country=="IS"?:""?>>Iceland</option>
            <option value="IN" <?php echo $country=="IN"?:""?>>India</option>
            <option value="ID" <?php echo $country=="ID"?:""?>>Indonesia</option>
            <option value="IR" <?php echo $country=="IR"?:""?>>Iran (Islamic Republic Of)</option>
            <option value="IQ" <?php echo $country=="IQ"?:""?>>Iraq</option>
            <option value="IE" <?php echo $country=="IE"?:""?>>Ireland</option>
            <option value="IL" <?php echo $country=="IL"?:""?>>Israel</option>
            <option value="IT" <?php echo $country=="IT"?:""?>>Italy</option>
            <option value="JM" <?php echo $country=="JM"?:""?>>Jamaica</option>
            <option value="JP" <?php echo $country=="JP"?:""?>>Japan</option>
            <option value="JO" <?php echo $country=="JO"?:""?>>Jordan</option>
            <option value="KZ" <?php echo $country=="KZ"?:""?>>Kazakhstan</option>
            <option value="KE" <?php echo $country=="KE"?:""?>>Kenya</option>
            <option value="KI" <?php echo $country=="KI"?:""?>>Kiribati</option>
            <option value="KP" <?php echo $country=="KP"?:""?>>Korea North</option>
            <option value="KR" <?php echo $country=="KR"?:""?>>Korea South</option>
            <option value="KW" <?php echo $country=="KW"?:""?>>Kuwait</option>
            <option value="KG" <?php echo $country=="KG"?:""?>>Kyrgyzstan</option>
            <option value="LA" <?php echo $country=="LA"?:""?>>Laos</option>
            <option value="LV" <?php echo $country=="LV"?:""?>>Latvia</option>
            <option value="LB" <?php echo $country=="LB"?:""?>>Lebanon</option>
            <option value="LS" <?php echo $country=="LS"?:""?>>Lesotho</option>
            <option value="LR" <?php echo $country=="LR"?:""?>>Liberia</option>
            <option value="LI" <?php echo $country=="LI"?:""?>>Liechtenstein</option>
            <option value="LT" <?php echo $country=="LT"?:""?>>Lithuania</option>
            <option value="LU" <?php echo $country=="LU"?:""?>>Luxembourg</option>
            <option value="MO" <?php echo $country=="MO"?:""?>>Macau</option>
            <option value="MK" <?php echo $country=="MK"?:""?>>Macedonia</option>
            <option value="MG" <?php echo $country=="MG"?:""?>>Madagascar</option>
            <option value="MW" <?php echo $country=="MW"?:""?>>Malawi</option>
            <option value="MY" <?php echo $country=="MY"?:""?>>Malaysia</option>
            <option value="MV" <?php echo $country=="MV"?:""?>>Maldives</option>
            <option value="ML" <?php echo $country=="ML"?:""?>>Mali</option>
            <option value="MT" <?php echo $country=="MT"?:""?>>Malta</option>
            <option value="MH" <?php echo $country=="MH"?:""?>>Marshall Islands</option>
            <option value="MQ" <?php echo $country=="MQ"?:""?>>Martinique</option>
            <option value="MR" <?php echo $country=="MR"?:""?>>Mauritania</option>
            <option value="MU" <?php echo $country=="MU"?:""?>>Mauritius</option>
            <option value="MX" <?php echo $country=="MX"?:""?>>Mexico</option>
            <option value="FM" <?php echo $country=="FM"?:""?>>Micronesia</option>
            <option value="MD" <?php echo $country=="MD"?:""?>>Moldova</option>
            <option value="MC" <?php echo $country=="MC"?:""?>>Monaco</option>
            <option value="MN" <?php echo $country=="MN"?:""?>>Mongolia</option>
            <option value="MS" <?php echo $country=="MS"?:""?>>Montserrat</option>
            <option value="MA" <?php echo $country=="MA"?:""?>>Morocco</option>
            <option value="MZ" <?php echo $country=="MZ"?:""?>>Mozambique</option>
            <option value="NA" <?php echo $country=="NA"?:""?>>Namibia</option>
            <option value="NP" <?php echo $country=="NP"?:""?>>Nepal</option>
            <option value="NL" <?php echo $country=="NL"?:""?>>Netherlands</option>
            <option value="AN" <?php echo $country=="AN"?:""?>>Netherlands Antilles</option>
            <option value="NC" <?php echo $country=="NC"?:""?>>New Caledonia</option>
            <option value="NZ" <?php echo $country=="NZ"?:""?>>New Zealand</option>
            <option value="NI" <?php echo $country=="NI"?:""?>>Nicaragua</option>
            <option value="NE" <?php echo $country=="NE"?:""?>>Niger</option>
            <option value="NG" <?php echo $country=="NG"?:""?>>Nigeria</option>
            <option value="NO" <?php echo $country=="NO"?:""?>>Norway</option>
            <option value="OM" <?php echo $country=="OM"?:""?>>Oman</option>
            <option value="PK" <?php echo $country=="PK"?:""?>>Pakistan</option>
            <option value="PW" <?php echo $country=="PW"?:""?>>Palau</option>
            <option value="PS" <?php echo $country=="PS"?:""?>>Palestine Autonomous</option>
            <option value="PA" <?php echo $country=="PA"?:""?>>Panama</option>
            <option value="PG" <?php echo $country=="PG"?:""?>>Papua New Guinea</option>
            <option value="PY" <?php echo $country=="PY"?:""?>>Paraguay</option>
            <option value="PE" <?php echo $country=="PE"?:""?>>Peru</option>
            <option value="PH" <?php echo $country=="PH"?:""?>>Philippines</option>
            <option value="PL" <?php echo $country=="PL"?:""?>>Poland</option>
            <option value="PT" <?php echo $country=="PT"?:""?>>Portugal</option>
            <option value="PR" <?php echo $country=="PR"?:""?>>Puerto Rico</option>
            <option value="QA" <?php echo $country=="QA"?:""?>>Qatar</option>
            <option value="RE" <?php echo $country=="RE"?:""?>>Reunion</option>
            <option value="RO" <?php echo $country=="RO"?:""?>>Romania</option>
            <option value="RU" <?php echo $country=="RU"?:""?>>Russian Federation</option>
            <option value="RW" <?php echo $country=="RW"?:""?>>Rwanda</option>
            <option value="VC" <?php echo $country=="VC"?:""?>>Saint Vincent and the Grenadines</option>
            <option value="MP" <?php echo $country=="MP"?:""?>>Saipan</option>
            <option value="SM" <?php echo $country=="SM"?:""?>>San Marino</option>
            <option value="SA" <?php echo $country=="SA"?:""?>>Saudi Arabia</option>
            <option value="SN" <?php echo $country=="SN"?:""?>>Senegal</option>
            <option value="SC" <?php echo $country=="SC"?:""?>>Seychelles</option>
            <option value="SL" <?php echo $country=="SL"?:""?>>Sierra Leone</option>
            <option value="SG" <?php echo $country=="SG"?:""?>>Singapore</option>
            <option value="SK" <?php echo $country=="SK"?:""?>>Slovak Republic</option>
            <option value="SI" <?php echo $country=="SI"?:""?>>Slovenia</option>
            <option value="SO" <?php echo $country=="SO"?:""?>>Somalia</option>
            <option value="ZA" <?php echo $country=="ZA"?:""?>>South Africa</option>
            <option value="ES" <?php echo $country=="ES"?:""?>>Spain</option>
            <option value="LK" <?php echo $country=="LK"?:""?>>Sri Lanka</option>
            <option value="KN" <?php echo $country=="KN"?:""?>>St. Kitts/Nevis</option>
            <option value="LC" <?php echo $country=="LC"?:""?>>St. Lucia</option>
            <option value="SD" <?php echo $country=="SD"?:""?>>Sudan</option>
            <option value="SR" <?php echo $country=="SR"?:""?>>Suriname</option>
            <option value="SZ" <?php echo $country=="SZ"?:""?>>Swaziland</option>
            <option value="SE" <?php echo $country=="SE"?:""?>>Sweden</option>
            <option value="CH" <?php echo $country=="CH"?:""?>>Switzerland</option>
            <option value="SY" <?php echo $country=="SY"?:""?>>Syria</option>
            <option value="TW" <?php echo $country=="TW"?:""?>>Taiwan</option>
            <option value="TI" <?php echo $country=="TI"?:""?>>Tajikistan</option>
            <option value="TZ" <?php echo $country=="TZ"?:""?>>Tanzania</option>
            <option value="TH" <?php echo $country=="TH"?:""?>>Thailand</option>
            <option value="TG" <?php echo $country=="TG"?:""?>>Togo</option>
            <option value="TK" <?php echo $country=="TK"?:""?>>Tokelau</option>
            <option value="TO" <?php echo $country=="TO"?:""?>>Tonga</option>
            <option value="TT" <?php echo $country=="TT"?:""?>>Trinidad and Tobago</option>
            <option value="TN" <?php echo $country=="TN"?:""?>>Tunisia</option>
            <option value="TR" <?php echo $country=="TR"?:""?>>Turkey</option>
            <option value="TM" <?php echo $country=="TM"?:""?>>Turkmenistan</option>
            <option value="TC" <?php echo $country=="TC"?:""?>>Turks and Caicos Islands</option>
            <option value="TV" <?php echo $country=="TV"?:""?>>Tuvalu</option>
            <option value="UG" <?php echo $country=="UG"?:""?>>Uganda</option>
            <option value="UA" <?php echo $country=="UA"?:""?>>Ukraine</option>
            <option value="AE" <?php echo $country=="AE"?:""?>>United Arab Emirates</option>
            <option value="UY" <?php echo $country=="UY"?:""?>>Uruguay</option>
            <option value="UZ" <?php echo $country=="UZ"?:""?>>Uzbekistan</option>
            <option value="VU" <?php echo $country=="VU"?:""?>>Vanuatu</option>
            <option value="VN" <?php echo $country=="VN"?:""?>>Viet Nam</option>
            <option value="VG" <?php echo $country=="VG"?:""?>>Virgin Islands (British)</option>
            <option value="VI" <?php echo $country=="VI"?:""?>>Virgin Islands (U.S.)</option>
            <option value="WF" <?php echo $country=="WF"?:""?>>Wallis and Futuna Islands</option>
            <option value="YE" <?php echo $country=="YE"?:""?>>Yemen</option>
            <option value="YU" <?php echo $country=="YU"?:""?>>Yugoslavia</option>
            <option value="ZM" <?php echo $country=="ZM"?:""?>>Zambia</option>
            <option value="ZW" <?php echo $country=="ZW"?:""?>>Zimbabwe</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Estado</label>
          <input name="state" id="state" type="text" class="form-control"  value="<?php echo $state;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Código Postal:</label>
          <input name="zip" id="zip" type="text" class="form-control"  value="<?php echo $zip;?>" onkeyup="checkFieldBack(this);" />
        </div>
      </div>
    </div>
    
    
    
  </div>
  <!-- BILLING BLOCK -->
  
  
  <!-- CREDIT CARD BLOCK -->
  <h4>Información de la tarjeta</h4>
  <div class="pane">
    <div class="form-group">
      <label> Tipo: </label>
      <input name="cctype" type="radio" value="V" class="lft-field" /> <img src="images/ico_visa.jpg" align="absmiddle" class="lft-field cardhide V" />
      <input name="cctype" type="radio" value="M" class="lft-field" /> <img src="images/ico_mc.jpg" align="absmiddle" class="lft-field cardhide M" />
      <input name="cctype" type="radio" value="A" class="lft-field" /> <img src="images/ico_amex.jpg" align="absmiddle" class="lft-field cardhide A" />
      <input name="cctype" type="radio" value="D" class="lft-field" /> <img src="images/ico_disc.jpg" align="absmiddle" class="lft-field cardhide D" />
      <?php if($enable_paypal){ ?>
      <input name="cctype" type="radio" value="PP" class="lft-field isPayPal"  /> <img src="images/ico_paypal.png" width="37" height="11"  align="absmiddle" class="lft-field paypal cardhide PP"  />
      <?php } ?>
    </div>
    
    <div class="ccinfo">
      <div class="form-group">
        <label>Nombre (como en la tarjeta):</label>
        <input name="ccname" id="ccname" type="text" class="form-control"  onkeyup="checkFieldBack(this);"  />
      </div>
      <div class="row">
        <div class="col-md-5">
          <div class="form-group">
            <label>Número:</label>
            <input name="ccn" id="ccn" type="text" class="form-control"  onkeyup="checkNumHighlight(this.value);checkFieldBack(this);noAlpha(this);" value="" onkeypress="checkNumHighlight(this.value);noAlpha(this);" onblur="checkNumHighlight(this.value);" onchange="checkNumHighlight(this.value);" maxlength="16" />
            <span class="ccresult"></span>
          </div>
        </div>
        
        <div class="col-md-7">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label>Fecha de Expiración:</label>
                <div class="row">
                  <div class="col-xs-6 pr5">
                    <select name="exp1" id="exp1" class="form-control" onchange="checkFieldBack(this);">
                      <option value="01">01</option>
                      <option value="02">02</option>
                      <option value="03">03</option>
                      <option value="04">04</option>
                      <option value="05">05</option>
                      <option value="06">06</option>
                      <option value="07">07</option>
                      <option value="08">08</option>
                      <option value="09">09</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                  </div>
                  <div class="col-xs-6 pl5">
                    <select name="exp2" id="exp2" class="form-control" onchange="checkFieldBack(this);">
                      <?php echo getActualYears();   ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <div class="row no-gutter">
                  <div class="col-xs-8">
                    <label>CVV:</label>
                    <input name="cvv" id="cvv" type="text" maxlength="5" class="form-control"  onkeyup="checkFieldBack(this);noAlpha(this);"  />
                    
                  </div>
                  <div class="col-xs-4">
                    <a href="#modal-hint" role="button" data-toggle="modal"><img class="hint" src="images/ico_question.jpg" align="absmiddle" border="0" /></a>
                   

                    <?php // Hint modal ?>

                   <div class="modal fade" id="modal-hint" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                     <div class="modal-dialog">
                       <div class="modal-content">
                         <div class="modal-header">
                           
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                           ×
                           </button>
                           <h4 class="modal-title" id="myModalLabel2">
                          CVV
                           </h4>
                         </div>
                         <div class="modal-body">
                          <p>
                            Para <strong>Visa</strong>, <strong>MasterCard</strong> y <strong>Discover</strong>, el código de la tarjeta es el último número de 3 dígitos situado en la parte posterior de su tarjeta en o por encima de su línea de firma.
                          </p>
                          <p> Para la tarjeta <strong>American Express</strong>, son los 4 dígitos en la parte frontal por encima de su número de tarjeta </p>
                               <img src="images/cvv_info.jpg" />
                         </div>
                         <div class="modal-footer">
                           
                           <button type="button" class="btn btn-default" data-dismiss="modal">
                           Cerrar
                           </button>
                         </div>
                       </div>
                     </div>
                   </div> 

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="submit-btn"><input type="submit" name="submit" value="Donar" class="btn btn-success btn-lg"></div>
    <input type="hidden" name="process" value="yes" />
    <div class="clearfix"></div>
  </div>
  <!-- CREDIT CARD BLOCK -->
  <?php } ?>
  
</div>
</form>
</div>