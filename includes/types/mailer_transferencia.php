<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.

        // Número de Transferencia: 
        // Monto:
        // Nombre: 
        // Email: 
        // Ciudad: 
        // Dirección:
        // País:
        // Estado:
        // Código Postal:
        // Mensaje: 

        $name = strip_tags(trim($_POST["name_trans"]));
		$name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email_trans"]), FILTER_SANITIZE_EMAIL);
        $transferencia = trim($_POST["transferencia"]);
        $message = trim($_POST["message_trans"]);

        $monto = trim($_POST["monto_transferencia"]);
        $message = trim($_POST["message_trans"]);
        $pais = trim($_POST["pais_transferencia"]);
        $ciudad = trim($_POST["ciudad_transferencia"]);
        $direccion = trim($_POST["direccion_transferencia"]);
        $estado = trim($_POST["estado_transferencia"]);
        $zip = trim($_POST["zip_transferencia"]);
        


        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Error: Hubo un problema con el envío, por favor complete el formulario y vuelve a intentarlo.";
            exit;
        }

        // Set the recipient email address.
        $recipient = "miguel.antekera@gmail.com";

        // Set the email subject.
        $subject = "Confirmación de transferencia bancaria | $transferencia";

        // Build the email content.
        $email_content = "Nombre: $name\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "No. de Transferencia:\n$transferencia\n";
        $email_content .= "Monto:\n$monto\n";
        $email_content .= "Mensaje:\n$message\n";
        $email_content .= "País:\n$pais\n";
        $email_content .= "Estado:\n$estado\n";
        $email_content .= "Ciudad:\n$ciudad\n";
        $email_content .= "Dirección:\n$direccion\n";
        $email_content .= "Código postal:\n$zip\n";

        // Build the email headers.
        $email_headers = "From: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Los datos de la transferencia han sido enviados, recibirás un email una vez sean confirmados.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Error: no fueron enviado los datos por favor intente de nuevo.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Hubo un problema con el envío, por favor intente de nuevo.";
    }

?>
