<div class="container">

  <div class="row">
    <div class="col-md-8"> 

      <p>
        Su donación podrá ser deducible de impuestos porque tenemos el registro especial 501(c)(3) en Estados Unidos.
      </p>
      <br />

      <div class="tabbable tabs" id="tabs-581297">
      
         <ul class="nav nav-tabs">
          <li class="active">
            <a href="#panel-tab1" data-toggle="tab">Tarjeta o Paypal</a>
          </li>
          <li>
            <a href="#panel-tab3" data-toggle="tab">Tranferencia Bancaria</a>
          </li>
        </ul>

        <div class="tab-content">
          <div class="tab-pane active" id="panel-tab1">

            <?php include "includes/javascript.validation.php"; ?>

            <div class="tab-content">
              <?php include "includes/types/form.php"; ?>
            </div>
            <!-- .tab-content 1 -->
            
          </div>
          <!-- .tab-pane 1 -->

          <div class="tab-pane" id="panel-tab3">
             
             <div class="tab-content">
               
               <?php include "includes/types/transferencia.php"; ?>
          
             </div>
             <!-- .tab-content 3 -->
          
           </div>
           <!-- .tab-pane 3 -->
         
        </div>
      </div>
      <!-- .tabs -->

    </div>
    <div class="col-md-4">
      <div class="placeholder side-image"> </div>
    </div>
  </div> 
