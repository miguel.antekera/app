<script type="text/JavaScript">
<!--

function checkForm() {
	var err=0;

    for (var i=0; i < document.ff1.cctype.length; i++){
       if (document.ff1.cctype[i].checked){
          var cctype = document.ff1.cctype[i].value;
        }
    }


<?php
$reqFields=array(
	"fname",
	"lname",
	"address",
	"city",
	"state",
	"zip",
	"email",
    "amount"
);

$reqFields_cc=array(
    "ccn",
    "ccname",
    "exp1",
    "exp2",
    "cvv"
);

if($show_services){
	$reqField[] = "service";
}

foreach ($reqFields as $v) { ?>
if (document.getElementById('<?php echo $v;?>').value==0) {
    if (err==0) {
        document.getElementById('<?php echo $v;?>').focus();
    }
    document.getElementById('<?php echo $v;?>').style.backgroundColor='#ffa5a5';
    err=1;
}
<?php } ?>

if(cctype!="PP"){
    <?php foreach ($reqFields_cc as $v) { ?>
            if (document.getElementById('<?php echo $v;?>').value==0) {
                if (err==0) {
                    document.getElementById('<?php echo $v;?>').focus();
                }
                document.getElementById('<?php echo $v;?>').style.backgroundColor='#ffa5a5';
                err=1;
            }
    <?php } ?>
    if(err==0){

        //check credit card.
        var ccn = document.getElementById("ccn").value;
        if(!isValidCardNumber(ccn)){
            alert("Número de tarjeta de crédito no válido. Por favor confirmelo y vuelva a intentarlo");
            return false;
        }
        if(isExpiryDate(document.getElementById("exp2").value,document.getElementById("exp1").value)==false){
            alert("La fecha de expiración está en el pasado, por favor cambiela e intente de nuevo.");
            return false;
        }

        if(!isCardTypeCorrect(ccn,cctype)){
            alert("Combinación de número/tipo de tarjeta de crédito es inválida. Por favor confirmelo y vuelva a intentarlo.");
            return false;
        }
    }
}





	var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
	var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,5}|[0-9]{1,3})(\]?)$/; // valid
	if (document.getElementById('email').value==0 || !reg2.test(document.getElementById('email').value)) {
	if (err==0) {
		document.getElementById('email').focus();
	}
	document.getElementById('email').style.backgroundColor='#ffa5a5';
	err=1;
	}

if (err==0) {
		return true;
	} else {
		alert("Por favor, complete todos los campos resaltados para continuar.");
		return false;
	}
}

function checkFieldBack(fieldObj) {
	if (fieldObj.value!=0) {
		fieldObj.style.backgroundColor='#F8F8F8';
	}
}
function noAlpha(obj){
	reg = /[^0-9.,]/g;
	obj.value =  obj.value.replace(reg,"");
 }


//-->
</script>