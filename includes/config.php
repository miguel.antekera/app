<?php

session_start();
error_reporting(E_ALL ^ E_NOTICE);
require("functions.php");
/*******************************************************************************************************
    GENERAL SCRIPT CONFIGURATION VARIABLES
********************************************************************************************************/
$title = "Donaciones"; //site title
$admin_email = "miguel.antekera@gmail.com"; //this email is for notifications about new payments
//CHANGE "USD" TO REQUIRED CURRENCY, SUPPORTED BY PROVIDER.USD, CAD, EUR
define("PTP_CURRENCY_CODE","USD"); 

// services not apply to donations
// $services = array(
// 				  array("Donación de:", "0.1")
// 			);
//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			
$show_services = false;

// set  to   RECUR  - for recurring payments, ONETIME - for 
$payment_mode = $mode_choosen;



//service name   |   price  to charge   | Billing period  "Day", "Week", "SemiMonth", "Month", "Year"   |  how many periods of previous field per billing period
// $recur_services = array(
// 				 array("Anual", "0.10", "Year", "1"),
// 				 array("Anual2", "20.00", "Year", "1"),
// 				 array("Anual3", "50.00", "Year", "1"),
// 				 array("Anual4", "100.00", "Year", "1"),
// 				); 

$year = "Year"; //"Day", "Week", "Month", "SemiMonth", "Year"

//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
// MAKE SURE SSL IS INSTALLED ALREADY.
$redirect_non_https = true;
$liveMode = true;
/****************************************************
//TEST CREDIT CARD CREDENTIALS for SANDBOX TESTING
Card Type: Visa
Account Number: 4683075410516684
Expiration Date: Any in future
Security Code: 123
****************************************************/

if(!$liveMode){
//TEST MODE
define('API_USERNAME', 'merchant-1_api1.transparencia.org');
define('API_PASSWORD', '5LVVDYTL3BCF6L3E');
define('API_SIGNATURE', 'A638yate8zfFa5k5Q11nkNj4ndkvAsUG-N4HZkg4CfAdgHCOw3NPpMMi');
define('API_ENDPOINT', 'https://api-3t.sandbox.paypal.com/nvp');
define('PAYPAL_URL', 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=');
} else {
//LIVE MODE
define('API_USERNAME', 'larense_api1.hushmail.com');
define('API_PASSWORD', 'BWKJSE8KULESSYM2');
define('API_SIGNATURE','AFcWxV21C7fd0v3bYYYRCpSSRl31A2wwJQOwGU48qAQjflBAcZv6-oW0');
//DONT EDIT BELOW 2 LINES IF UNSURE.
define('API_ENDPOINT', 'https://api-3t.paypal.com/nvp');
define('PAYPAL_URL', 'https://www.paypal.com/webscr&cmd=_express-checkout&token=');
}

/*******************************************************************************************************
    PAYPAL EXPRESS CHECKOUT CONFIGURATION VARIABLES
********************************************************************************************************/
$enable_paypal = true;  
$paypal_merchant_email = "miguel.antekera@gmail.com";
$paypal_success_url = "https:/transparencia.org.ve/donate_user/paypal-pro-terminal/paypal_thankyou.php";
$paypal_cancel_url = "https:/transparencia.org.ve/donate_user/paypal-pro-terminal/paypal_cancel.php";
$paypal_ipn_listener_url = "https:/transparencia.org.ve/donate_user/paypal-pro-terminal/paypal_listener.php";
$paypal_custom_variable = "tv_";
$paypal_currency = "USD";
$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)
if($liveMode){ $sandbox = false; } else { $sandbox = true; }


//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
define("PAYMENT_MODE",$payment_mode);
if(!$sandbox){
    define("PAYPAL_URL_STD","https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_URL_STD","https://www.sandbox.paypal.com/cgi-bin/webscr");
}

define('USE_PROXY',FALSE);
define('PROXY_HOST', '127.0.0.1');
define('PROXY_PORT', '808');
define('VERSION', '2.3');
define('ACK_SUCCESS', 'SUCCESS');
define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');

if($redirect_non_https){
	if ($_SERVER['SERVER_PORT']!=443) {
		$sslport=443; //whatever your ssl port is
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("Location: $url");
		exit();
	}
}
?>