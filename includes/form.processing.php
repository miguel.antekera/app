<?php

    $province = str_replace("-AU-", "", $state);

        if ($show_services) {
            if($payment_mode=="RECUR"){
                $amount = number_format($recur_services[$service][1], 2);
            } else {
                $amount = number_format($services[$service][1], 2);
            }
            $item_description = $services[$service][0];
        }

		$continue = false;
		if(!empty($amount) && is_numeric($amount)){ 	
			$cctype = (!empty($_POST['cctype']))?strip_tags(str_replace("'","`",strip_tags($_POST['cctype']))):'';
			$ccname = (!empty($_POST['ccname']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccname']))):'';
			$ccn = (!empty($_POST['ccn']))?strip_tags(str_replace("'","`",strip_tags($_POST['ccn']))):'';
			$exp1 = (!empty($_POST['exp1']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp1']))):'';
			$exp2 = (!empty($_POST['exp2']))?strip_tags(str_replace("'","`",strip_tags($_POST['exp2']))):'';
			$cvv = (!empty($_POST['cvv']))?strip_tags(str_replace("'","`",strip_tags($_POST['cvv']))):'';
			
			
            if($cctype!="PP"){
                //CREDIT CARD PHP VALIDATION
                if(empty($ccn) || empty($cctype) || empty($exp1) || empty($exp2) || empty($ccname) || empty($cvv) || empty($address) || empty($state) || empty($city)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> No fueron completados todos los campos requeridos.</p></div></div><br />';
                } else { $continue = true; }

                if(!is_numeric($cvv)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> Número CVV puede contener sólo números.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if(!is_numeric($ccn)){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> El número de tarjeta de crédito sólo puede contener números.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if(date("Y-m-d", strtotime($exp2."-".$exp1."-01")) < date("Y-m-d")){
                    $continue = false;
                    $mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> Su tarjeta de crédito ha caducado.</p></div></div><br />';
                } else {
                    $continue = true;
                }

                if($continue){
                    //echo "1";
                    if(validateCC($ccn,$cctype)){
                        $continue = true;
                    } else {
                        $continue = false;
                        $mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> El número que ha introducido no coincide con el tipo de tarjeta seleccionado.</p></div></div><br />';
                    }
                }

                if($continue){
                    if(luhn_check($ccn)){
                        $continue = true;
                    } else {
                        $continue = false;
                        $mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> Número de tarjeta de crédito no válido.</p></div></div><br />';
                    }
                }
            } else {
                $continue = true;
            }
			
            switch ($cctype) {
                case "V":
                    $cctype = "VISA";
                    break;
                case "M":
                    $cctype = "MASTERCARD";
                    break;
                case "DI":
                    $cctype = "DINERS CLUB";
                    break;
                case "D":
                    $cctype = "DISCOVER";
                    break;
                case "A":
                    $cctype = "AMEX";
                    break;
                case "PP":
                    $cctype = "PAYPAL";
                break;
            }
            $transactID = time()."-".rand(1,999);
            // $transactID = mktime()."-".rand(1,999);

            if($continue && $cctype!="PAYPAL"){
				require_once 'includes/paypal.callerservice.php';





				switch($payment_mode){

                    /*******************************************************************************************************
                    ONE TIME PAYMENT PROCESSING
                    *******************************************************************************************************/
                    case "ONETIME":
					$paymentType =urlencode("Sale");
                    $API_UserName=API_USERNAME;
                    $API_Password=API_PASSWORD;
                    $API_Signature=API_SIGNATURE;
                    $API_Endpoint =API_ENDPOINT;
                    //$subject = SUBJECT;
                    $paymentType =urlencode("Sale");
                    //CREDIT CARD INFO
                    $tt = explode(" ",trim($ccname));
                    if(is_array($tt)){
                        $firstName =urlencode( $tt[0]);
                        if(isset($tt[2])){ $temp = $tt[1]." ".$tt[2]; } else { if(isset($tt[1])){ $temp = $tt[1]; } else { $temp = ""; } }
                        $lastName =urlencode( $temp );
                    } else {
                        $firstName =urlencode( $ccname);
                        $lastName =urlencode("");
                    }
                    $creditCardType =urlencode($cctype);
                    $creditCardNumber = urlencode(trim($ccn));
                    $expDateMonth =urlencode( $exp1);
                    $padDateMonth = str_pad($exp1, 2, '0', STR_PAD_LEFT);
                    $expDateYear =urlencode(  $exp2);
                    $cvv2Number = urlencode(trim($cvv));

                    //CUSTOMER INFO
                    $address1 = urlencode($address);
                    $countryCode = urlencode($country);
                    $city = urlencode($city);
                    $state =urlencode( $state);
                    $zip = urlencode($zip);

                    $amount = urlencode(number_format($amount,2));
                    //$amount = urlencode($amount);

					$currencyCode=PTP_CURRENCY_CODE;
					/* Construct the request string that will be sent to PayPal.
					   The variable $nvpstr contains all the variables and is a
					   name value pair string with & as a delimiter */
					$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$countryCode&CURRENCYCODE=$currencyCode";
					$getAuthModeFromConstantFile = true;
					$nvpHeader = "";
					
					if(!$getAuthModeFromConstantFile) {
						$AuthMode = "3TOKEN"; //Merchant's API 3-TOKEN Credential is required to make API Call.
						//$AuthMode = "FIRSTPARTY"; //Only merchant Email is required to make EC Calls.
						//$AuthMode = "THIRDPARTY"; //Partner's API Credential and Merchant Email as Subject are required.
					} else {
						if(!empty($API_UserName) && !empty($API_Password) && !empty($API_Signature) && !empty($subject)) {
							$AuthMode = "THIRDPARTY";
						}else if(!empty($API_UserName) && !empty($API_Password) && !empty($API_Signature)) {
							$AuthMode = "3TOKEN";
						}else if(!empty($subject)) {
							$AuthMode = "FIRSTPARTY";
						}
					}
					
					switch($AuthMode) {
						
						case "3TOKEN" : 
								$nvpHeader = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature);
								break;
						case "FIRSTPARTY" :
								$nvpHeader = "&SUBJECT=".urlencode($subject);
								break;
		
						case "THIRDPARTY" :
								$nvpHeader = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature)."&SUBJECT=".urlencode($subject);
								break;		
						
					}
					
					$nvpstr = $nvpHeader.$nvpstr;
					//echo $nvpstr;
					/* Make the API call to PayPal, using API signature.
					   The API response is stored in an associative array called $resArray */
					$resArray=hash_call("doDirectPayment",$nvpstr);
					
					/* Display the API response back to the browser.
					   If the response from PayPal was a success, display the response parameters'
					   If the response was an error, display the errors received using APIError.php.
					   */
					$ack = strtoupper($resArray["ACK"]);
					
					if($ack!="SUCCESS" && $ack!="SUCCESSWITHWARNING")  {
						$_SESSION['reshash']=$resArray;
						$resArray=$_SESSION['reshash']; 
						if(isset($_SESSION['curl_error_no'])) { 
							$errorCode= $_SESSION['curl_error_no'] ;
							$errorMessage=$_SESSION['curl_error_msg'] ;
                            $my_status = "<div>Transacción sin éxito.<br/>";
                            $my_status .= "Se ha producido un error al procesar la tarjeta de crédito:<br/>";
                            $my_status .= "Respuesta del gateway: " . $errorMessage . ", Error: " . $errorCode . "<br/>";
                            $my_status .= "</div>";
                            $error = 1;
                            $mess = '<div class="ui-widget"><div class="alert alert-danger fade in">' . $my_status . '</div></div><br />';

                        } else {
							$count=0;
							$my_status="<div>Transacción sin éxito.<br/>";
							$my_text="Se ha producido un error al procesar la tarjeta de crédito:<br/>";
							while (isset($resArray["L_SHORTMESSAGE".$count])) {		
								$errorCode    = $resArray["L_ERRORCODE".$count];
								$shortMessage = $resArray["L_SHORTMESSAGE".$count];
								$longMessage  = $resArray["L_LONGMESSAGE".$count]; 
								$count=$count+1; 					
								$my_text.="Código de error: ".$errorCode."<br/>";
								$my_text.="Mensaje de error: ".$longMessage."<br/>";
							}//end while
							$my_status .= $my_text."</div>";
							$error=1;
							$mess = '<div class="ui-widget"><div class="alert alert-danger fade in">'.$my_status.'</div></div><br />';
						}// end else
					 } else { 
							$my_status="<br/><div>Transacción Exitosa<br/>";
							$my_status .="Gracias por tu donación<br /><br />";
                            $my_status .= "ID: " . $resArray["TRANSACTIONID"] . "<br />";
							$my_status .= "En breve recibiras un correo de confirmación.<br/><br/><a href='index.php'>Regresar</a></div><br/>";
							$error=0;
							$mess = '<div class="ui-widget"><div class="alert alert-success fade in">'.$my_status.'</div></div><br />';
							#**********************************************************************************************#
							#		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
							#**********************************************************************************************#
							
							#**********************************************************************************************#
                            /******************************************************************
                            ADMIN EMAIL NOTIFICATION
                            ******************************************************************/
                            $headers = "MIME-Version: 1.0\n";
                            $headers .= "Content-type: text/html; charset=utf-8\n";
                            $headers .= "From: 'Transparencia Venezuela' <noreply@" . $_SERVER['HTTP_HOST'] . "> \n";
                            $subject = "Nueva Donación Recibida";
                            $message = "Una nueva donación ha sido recibida. <br />";
                            $message .= "From " . $fname . " " . $lname . "  el " . date('m/d/Y') . " a las " . date('g:i A') . ".<br /> Monto: $" . number_format($amount, 2);
                            if ($show_services) {
                                $message .= "<br />Donado por: \"" . $services[$service][0] . "\"";
                            } else {
                                $message .= "<br />Descripción \"" . $item_description . "\"";
                            }
                            $message .= "<br />Número de transacción: \"" . $resArray["TRANSACTIONID"] . "\"";
                            $message .= "<br /><br />Información:<br />";
                            $message .= "Nombre: " . $fname . " " . $lname . "<br />";
                            $message .= "Email: " . $email . "<br />";
                            $message .= "Dirección: " . $address . "<br />";
                            $message .= "Ciudad: " . $city . "<br />";
                            $message .= "País: " . $country . "<br />";
                            $message .= "Estado: " . $state . "<br />";
                            $message .= "Código Postal: " . $zip . "<br />";

                            mail($admin_email, $subject, $message, $headers);
							
                            /******************************************************************
                            CUSTOMER EMAIL NOTIFICATION
                            ******************************************************************/
                            $subject = "Donación recibida";
                            $message = "Estimado " . $fname . "  " . $lname . ",<br />";
                            $message .= "<br /> ¡Muchas gracias por tu donación!";
                            $message .= "<br /><br />";
                            // if ($show_services) {
                            //     $message .= "<br />Donado por: \"" . $services[$service][0] . "\"";
                            // } else {
                            //     $message .= "<br />Donado por: \"" . $item_description . "\"";
                            // }
                            $message .= "<br />Muchas gracias por su generosa donación de US $" . number_format($amount, 2);

                            $message .= "<br /> Su apoyo es un gran insumo para Transparencia Venezuela ya que le permite 
                            continuar su trabajo por un país libre de corrupción. Su aporte le da poder a los ciudadanos en 
                            todo el país para denunciar actos de corrupción a través de la plataforma ‘Dilo Aquí’ y para que 
                            se haga justicia. A través de nuestro programa de Asistencia Legal Anticorrupción ofrecemos 
                            asesoría legal gratuita y confidencial a testigos y víctimas de corrupción a todos los niveles, 
                            desde pequeños actos de soborno a hechos de corrupción a gran escala en Venezuela.<br /><br />";

                            $message .= "<br /> En caso de que tenga alguna pregunta sobre su donación o le gustaría recibir 
                            más información sobre nuestro trabajo, por favor no dude en consultarnos por 
                            antiguiso@transparencia.org.ve o visite nuestra página web https://www.transparencia.org.ve. 
                            También le invitamos a que participe en nuestras redes Facebook y Twitter.<br /><br />";

                            $message .= "<br /> Atentamente,<br />
                            Equipo Transparencia Venezuela”";



                            $message .= "<br /><br />Número de transacción: \"" . $resArray["TRANSACTIONID"] . "\"";
                            $message .= "<br /><br />nformación:<br />";
                            $message .= "Nombre " . $fname . " " . $lname . "<br />";
                            $message .= "Email: " . $email . "<br />";
                            $message .= "Direción: " . $address . "<br />";
                            $message .= "Ciudad " . $city . "<br />";
                            $message .= "País: " . $country . "<br />";
                            $message .= "Estado: " . $state . "<br />";
                            $message .= "Código Postal: " . $zip . "<br />";

                            $message .= "<br /><br />Atentamente,<br />" . $_SERVER['HTTP_HOST'];
                            mail($email, $subject, $message, $headers);

                            //-----> send notification end
                            $show_form = 0;
							}
						break;
				case "RECUR":
                    /*******************************************************************************************************
                    RECURRING PROCESSING
                    *******************************************************************************************************/
					
					$token = urlencode("");
					$paymentAmount = urlencode($amount);
					$currencyID = urlencode(PTP_CURRENCY_CODE);		// or other paypal supported currency
					$startDate = urlencode(date("Y-m-d")."T".date("G:i:s")); // "2009-9-6T0:0:0"date("Y-m-d G:i:s")
					$billingPeriod = urlencode($year);
                    // $billingPeriod = urlencode($recur_services[$service][2]);	// or "Day", "Week", "Month", "SemiMonth", "Year"
					$billingFreq = urlencode($year);
                    // $billingFreq = urlencode($recur_services[$service][3]);		// combination of this and billingPeriod must be at most a year
					
					$cartType = urlencode($cctype);
					$cartNumber = urlencode($ccn);
					$expDate = urlencode($exp1.$exp2);
					$fname = urlencode($fname);
					$lname = urlencode($lname);
					// $desc = urlencode($recur_services[$service][0]);
                    $desc = urlencode($service);
					
					$nvpStr="&TOKEN=$token&AMT={$paymentAmount}&CURRENCYCODE={$currencyID}&PROFILESTARTDATE={$startDate}";
					$nvpStr .= "&BILLINGPERIOD={$billingPeriod}&BILLINGFREQUENCY={$billingFreq}&DESC={$desc}";
					$nvpStr .= "&CREDITCARDTYPE={$cartType}&ACCT={$cartNumber}&EXPDATE={$expDate}&FIRSTNAME={$fname}&LASTNAME={$lname}&EMAIL={$email}";
					
					//print $nvpStr."<br><br>" ;
					$httpParsedResponseAr = PPHttpPost('CreateRecurringPaymentsProfile', $nvpStr);
					//print var_dump($httpParsedResponseAr);
					if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
						//exit('CreateRecurringPaymentsProfile Completed Successfully: '.print_r($httpParsedResponseAr, true));
							$my_status="<br/><div>Donación periódica creada con exito<br/>";
                            $my_status .= "ID: " . urldecode($httpParsedResponseAr["PROFILEID"]) . "<br />";
							$my_status .="Gracias por tu donación<br /><br />";
							$my_status .= "En breve recibirás un email de confirmación.<br/><br/><a href='index.php'>Volver</a></div><br/>";
							$error=0;
							$mess = '<div class="ui-widget"><div class="alert alert-success fade in">'.$my_status.'</div></div><br />';
							#**********************************************************************************************#
							#		THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS.
							#**********************************************************************************************#
							
							#**********************************************************************************************#
							//-----> send notification 
							//creating message for sending
							$headers  = "MIME-Version: 1.0\n";
							$headers .= "Content-type: text/html; charset=utf-8\n";
							$headers .= "De: 'Transparencia Internacional' <noreply@".$_SERVER['HTTP_HOST']."> \n";
							$subject = "Nueva donación periódica";
							$message =  "Nuevo donación periódica fue recibida con éxito a través de PayPal <br />";					
							$message .= "From ".$fname." ".$lname."  el ".date('m/d/Y')." a las ".date('g:i A').".<br /> Monto: $".number_format($amount,2);
							// if($show_services){
							// 	$message .= "<br />  \"".$recur_services[$service][0]."\"";
							// } else { 
							// 	$message .= "<br />Descripción \"".$item_description."\"";
							// }

                            $message .= "<br />Muchas gracias por su generosa donación de US $" . number_format($amount, 2);

                            $message .= "<br /> Su apoyo es un gran insumo para Transparencia Venezuela ya que le permite 
                            continuar su trabajo por un país libre de corrupción. Su aporte le da poder a los ciudadanos en 
                            todo el país para denunciar actos de corrupción a través de la plataforma ‘Dilo Aquí’ y para que 
                            se haga justicia. A través de nuestro programa de Asistencia Legal Anticorrupción ofrecemos 
                            asesoría legal gratuita y confidencial a testigos y víctimas de corrupción a todos los niveles, 
                            desde pequeños actos de soborno a hechos de corrupción a gran escala en Venezuela.<br /><br />";

                            $message .= "<br /> En caso de que tenga alguna pregunta sobre su donación o le gustaría recibir 
                            más información sobre nuestro trabajo, por favor no dude en consultarnos por 
                            antiguiso@transparencia.org.ve o visite nuestra página web https://www.transparencia.org.ve. 
                            También le invitamos a que participe en nuestras redes Facebook y Twitter.<br /><br />";

                            $message .= "<br /> Atentamente,<br />
                            Equipo Transparencia Venezuela”";

							$message .= "<br/>Inicia: ".date("Y-m-d")."<br />";
							$message .= "Periodo: ".$billingPeriod."<br />";
							$message .= "Frecuencia: ".$billingFreq."<br />";
							$message .= "ID: ".urldecode($httpParsedResponseAr['PROFILEID'])."<br />";
							
							$message .= "<br /><br />Información:<br />";
							$message .= "Nombre ".$fname." ".$lname."<br />";
							$message .= "Email: ".$email."<br />";
							$message .= "Direción: ".$address."<br />";
							$message .= "Ciudad ".$city."<br />";
							$message .= "País: ".$country."<br />";
							$message .= "Estado: ".$state."<br />";
							$message .= "Código Postal: ".$zip."<br />";
							
							
							mail($admin_email,$subject,$message,$headers);
							
							$subject = "Donación recibida";
							$message =  "Hola ".$fname.",<br />";
							$message .= "<br /> Gracias Por tu donación anual.";
                            if($show_services){
                                $message .= "<br />Donado por: \"".$recur_services[$service][0]."\"";
                            } else {
                                $message .= "<br />Descripción \"".$item_description."\"";
                            }
                            $message .= "<br/>Inicia: ".date("Y-m-d")."<br />";
                            $message .= "Periodo: ".$billingPeriod."<br />";
                            $message .= "Frecuencia: ".$billingFreq."<br />";
                            $message .= "ID: ".urldecode($httpParsedResponseAr['PROFILEID'])."<br />";
                            $message .= "<br /><br />nformación:<br />";
                            $message .= "Nombre ".$fname." ".$lname."<br />";
                            $message .= "Email: ".$email."<br />";
                            $message .= "Direción: ".$address."<br />";
                            $message .= "Ciudad ".$city."<br />";
                            $message .= "País: ".$country."<br />";
                            $message .= "Estado: ".$state."<br />";
                            $message .= "Código Postal: ".$zip."<br />";
							$message .= "<br /><br />Atentamente,<br />".$_SERVER['HTTP_HOST'];
							mail($email,$subject,$message,$headers);	
							
							//-----> send notification end 	
							$show_form=0;
					} else  {
						//exit('CreateRecurringPaymentsProfile failed: ' . print_r($httpParsedResponseAr, true));
							$count=0;
							$my_status="<div>Transacción sin éxito<br/>";
							$my_text="Se ha producido al procesar la tarjeta de crédito:<br/>";
							
							$my_status .="Error: ". urldecode($httpParsedResponseAr['L_LONGMESSAGE0'])."</div>";
							$error=1;
							$mess = '<div class="ui-widget"><div class="alert alert-danger fade in">'.$my_status.'</div></div><br />';
					}
					
					break;
					}
				 
			} else if($continue && $cctype=="PAYPAL"){
                require('includes/paypal.class.php');
                $paypal = new paypal_class;

                $paypal->add_field('business', $paypal_merchant_email);
                $paypal->add_field('return', $paypal_success_url);
                $paypal->add_field('cancel_return', $paypal_cancel_url);
                $paypal->add_field('notify_url', $paypal_ipn_listener_url);

                    if($payment_mode=="ONETIME"){
                        if($show_services){
                            $paypal->add_field('item_name_1', strip_tags(str_replace("'","",$services[$service][0])));
                        } else {
                            $paypal->add_field('item_name_1', strip_tags(str_replace("'","",$item_description)));
                        }
                        $paypal->add_field('amount_1', $amount);
                        $paypal->add_field('item_number_1', $transactID);
                        $paypal->add_field('quantity_1', '1');
                        $paypal->add_field('custom', $paypal_custom_variable);
                        $paypal->add_field('upload', 1);
                        $paypal->add_field('cmd', '_cart');
                        $paypal->add_field('txn_type', 'cart');
                        $paypal->add_field('num_cart_items', 1);
                        $paypal->add_field('payment_gross', $amount);
                        $paypal->add_field('currency_code',$paypal_currency);

                    } else if($payment_mode=="RECUR"){
                        if($show_services){
                            $paypal->add_field('item_name', strip_tags(str_replace("'","",$recur_services[$service][0])));
                        } else {
                            $paypal->add_field('item_name', strip_tags(str_replace("'","",$item_description)));
                        }
                        $paypal->add_field('item_number', $transactID);
                        $paypal->add_field('a3', $amount);
                        $paypal_duration = getDurationPaypal($recur_services[$service][2]); //get duration based on recurring_services array
                        $paypal->add_field('p3', $recur_services[$service][3]);
                        $paypal->add_field('t3', (is_array($paypal_duration)?$paypal_duration[0]:$paypal_duration));
                        $paypal->add_field('src', '1');
                        $paypal->add_field('no_note', '1');
                        $paypal->add_field('no_shipping', '1');
                        $paypal->add_field('custom', $paypal_custom_variable);
                        $paypal->add_field('currency_code',$paypal_currency);
                    }
                    $show_form=0;
                    $mess = $paypal->submit_paypal_post(); // submit the fields to paypal


            }
				
		} elseif(!is_numeric($amount) || empty($amount)) { 
			if($show_services){
				$mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> Por favor, seleccione el tipo de donación.</p></div></div><br />';
			} else { 
				$mess = '<div class="ui-widget"><div class="alert alert-danger fade in"><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><strong>Error:</strong> Por favor, escriba el monto de la donación</p></div></div><br />';
			}
			$show_form=1; 
		} 
?>