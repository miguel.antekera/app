<html lang="es">
  <head>
     <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Transparencia Venezuela | <?php echo $title?></title>

    <meta name="description" content="">

    <link rel="shortcut icon" href="images/favicon.png">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ccvalidations.js"></script>
    <script src="js/scripts.js"></script>
    

    <noscript>
    <style>
      .noscriptCase { display:none; }
    </style>
    </noscript>
  </head>
  <body>