<?
		require_once 'includes/paypal.callerservice.php';
		###########################################################################
		###	ManageRecurringPaymentsProfileStatus 
		###########################################################################
					
		$profileID = urlencode($profileID);
		$action = urlencode($action);
		$desc = urlencode($description);
					
		$nvpStr="&PROFILEID={$profileID}&ACTION={$action}&NOTE={$desc}";
					
					
		//print $nvpStr."<br><br>" ;
		$httpParsedResponseAr = PPHttpPost('ManageRecurringPaymentsProfileStatus', $nvpStr);
		
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
			//exit('CreateRecurringPaymentsProfile Completed Successfully: '.print_r($httpParsedResponseAr, true));
			$my_status="<br/><div>Perfil actualizado con éxito<br/>";
			$my_status .= "El perfil {$profileID} fue {$action}<br/><br/>";
			$error=0;
			$mess = '<div class="ui-widget"><div class="alert alert-success fade in" >'.$my_status.'</div></div><br />';
		}else{
			$my_status="<br/><div>Error actualizado con éxito.<br/>";
			$my_status .= "Perfil {$profileID} fue {$action}<br/>";
			$my_status .="Error: ". urldecode($httpParsedResponseAr['L_LONGMESSAGE0'])."<br/><br/></div>";
			$error=1;
			$mess = '<div class="ui-widget"><div class="alert alert-danger fade in" >'.$my_status.'</div></div><br />';
		}
?>