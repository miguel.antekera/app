<?php
  //REQUIRE CONFIGURATION FILE
  require("includes/pago_anual.php"); 

  //DEFAULT PARAMETERS FOR FORM [!DO NOT EDIT!]
  $show_form=1;
    if(!isset($mess)){ $mess = ""; }
  //REQUEST VARIABLES 
  $item_description = (!empty($_REQUEST["item_description"]))?strip_tags(str_replace("'","`",$_REQUEST["item_description"])):'';
  $amount = (!empty($_REQUEST["amount"]))?strip_tags(str_replace("'","`",$_REQUEST["amount"])):'';
  $fname = (!empty($_REQUEST["fname"]))?strip_tags(str_replace("'","`",$_REQUEST["fname"])):'';
  $lname = (!empty($_REQUEST["lname"]))?strip_tags(str_replace("'","`",$_REQUEST["lname"])):'';
  $email = (!empty($_REQUEST["email"]))?strip_tags(str_replace("'","`",$_REQUEST["email"])):'';
  $address = (!empty($_REQUEST["address"]))?strip_tags(str_replace("'","`",$_REQUEST["address"])):'';
  $city = (!empty($_REQUEST["city"]))?strip_tags(str_replace("'","`",$_REQUEST["city"])):'';
  $country = (!empty($_REQUEST["country"]))?strip_tags(str_replace("'","`",$_REQUEST["country"])):'US';
  $state = (!empty($_REQUEST["state"]))?strip_tags(str_replace("'","`",$_REQUEST["state"])):'';
  $zip = (!empty($_REQUEST["zip"]))?strip_tags(str_replace("'","`",$_REQUEST["zip"])):'';
  $service = (!empty($_REQUEST['service']))?strip_tags(str_replace("'","`",strip_tags($_REQUEST['service']))):'0';
  
  //FORM SUBMISSION PROCESSING 
  if(!empty($_POST["process"]) && $_POST["process"]=="yes"){
    require("includes/form.processing.php");
  }  
  //REQUIRE SITE HEADER TEMPLATE    
  require "includes/site.header.php"; 
?> 

<div class="page_class anual hidden">  </div>

<?php require "includes/site.content.php"; ?>

<?php require "includes/site.footer.php"; ?>