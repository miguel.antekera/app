<?php
  
  //REQUIRE CONFIGURATION FILE
  require("includes/config.php");
  //DEFAULT PARAMETERS FOR FORM [!DO NOT EDIT!]
  $show_form=1;
  $mess="";
  //REQUEST VARIABLES
  $profileID = (!empty($_REQUEST["profileID"]))?strip_tags(str_replace("'","`",$_REQUEST["profileID"])):'';
  $action = (!empty($_REQUEST["action"]))?strip_tags(str_replace("'","`",$_REQUEST["action"])):'';
  $description = (!empty($_REQUEST["description"]))?strip_tags(str_replace("'","`",$_REQUEST["description"])):'';
  //FORM SUBMISSION PROCESSING
  if(!empty($_POST["process"]) && $_POST["process"]=="yes"){
    require("includes/form.processing.recurring.php");
  }
      //REQUIRE SITE HEADER TEMPLATE
  require "includes/site.header.php";
?>

<div class="container">
  
<div class="row">

<div class="col-sm-6 col-sm-offset-3">
  <h3 class="text-center">Administar Perfil de Pago Anual</h3> 
  <form id="ff1" name="ff1" method="post" action="" enctype="multipart/form-data"  class="pppt_form">
    <?php echo $mess; ?>
    <div id="accordion">
      <?php if($show_form){ ?>
      <!-- PAYMENT BLOCK -->
      <h4 class="current text-center">Información</h4> <br />
      <div class="pane" style="display:block">
        
        
        <div class="form-group">
          <label>ID:</label>
          <input name="profileID" id="profileID" type="text" class="form-control" value="<?php echo $profileID;?>"  onkeyup="checkFieldBack(this);noAlpha(this);" onkeypress="noAlpha(this);" />
        </div>
        
        
        <div class="form-group">
        <label>Acción:</label>
        <select onchange="checkFieldBack(this)" class="form-control" id="action" name="action">
          <option value="">Selecciona</option>
          <option value="Cancel">Cancelar </option>
          <option value="Suspend">Suspender </option>
          <option value="Reactivate">Reactivar</option> 
        </select>
        </div>
        
        <div class="form-group">
        <label>Descripción:</label>
        <input name="description" id="description" type="text" class="form-control"  value="<?php echo $description;?>" onkeyup="checkFieldBack(this);" />
        </div>

        <div class="form-group"><input type="submit" name="submit" class="btn btn-default"/></div>
        <input type="hidden" name="process" value="yes" />
        
      </div>
      <!-- PAYMENT BLOCK -->
      <?php } ?>
      
    </div>
  </form>
</div>

</div>  


<?php require "includes/site.footer.php"; ?>  