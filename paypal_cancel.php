<?php
	
	//REQUIRE CONFIGURATION FILE
	require("includes/config.php");  

	//REQUIRE SITE HEADER TEMPLATE		
	require "includes/site.header.php"; 
?>
<div align="center" class="wrapper">
    <div class="form_container">
    	<h1>Donación cancelada</h1>
            <div id="accordion">
                <p>Ha cancelado la donación hecha con PayPal.<br /><br /><a href="index.php">Regresar</a></p>
            </div>
    </div>
</div>
<?php require "includes/site.footer.php"; ?>