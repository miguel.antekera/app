<?php
	
	//REQUIRE CONFIGURATION FILE
	require("includes/config.php");  

	//REQUIRE SITE HEADER TEMPLATE		
	require "includes/site.header.php"; 
?>
<div align="center" class="wrapper">
    <div class="form_container">
    	<h1>¡Gracias por tu donación!</h1>
            <div id="accordion">
                <p>Tu donación ha sido recibida y recibirás un código de confirmación en breve.<br /><br />
                    <a href="index.php">Volver</a></p>
            </div>
    </div>
</div>
<?php require "includes/site.footer.php"; ?>