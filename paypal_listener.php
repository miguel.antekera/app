<?php
	
	//REQUIRE CONFIGURATION FILE
	require("includes/config.php"); 
    require('includes/paypal.class.php');  // include the class file
    $paypal = new paypal_class;             // initiate an instance of the class
    $paypal->paypal_url = PAYPAL_URL;     // paypal url

if ($paypal->validate_ipn()) {
    if(isset($paypal->pp_data["txn_type"]) && strtolower($paypal->pp_data["txn_type"])=="subscr_cancel"){
        //paypal subscription cancellation email to customer
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: 'Transparencia Venezuela' <noreply@" . $_SERVER['HTTP_HOST'] . "> \n";
        $subject = "Donación periódica cancelada";
        $message = "Hola ".$paypal->pp_data["first_name"]." ".$paypal->pp_data["last_name"].", <br /> Sólo queríamos hacerle saber que su donación periódicaa Transparencia Venezuela se ha cancelado.<br /><br />";
        $message .= "Tipo de donación:".$paypal->pp_data["item_name"]."<br />";
        $message .= "<br /><br />Atentamente,<br />Transparencia Venezuela";
        mail($paypal->pp_data['payer_email'], $subject, $message, $headers);

        //paypal subscription cancellation email to admin
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: 'Transparencia Venezuela' <noreply@" . $_SERVER['HTTP_HOST'] . "> \n";
        $subject = "Donación periódica cancelada";
        $message = "Hola, <br /> Sólo queríamos hacerle saber que esta donación periódica se ha cancelado.<br /><br />";
        $message .= "Tipo de donación:".$paypal->pp_data["item_name"]."<br />";
        $message .= "Nombre:".$paypal->pp_data["first_name"]." ".$paypal->pp_data["last_name"]."<br />";
        $message .= "Email:".$paypal->pp_data['payer_email']."<br />";
        $message .= "<br /><br />Atentamente,<br />Transparencia Venezuela";
        mail($paypal->pp_data['payer_email'], $subject, $message, $headers);

    } else if(isset($paypal->pp_data["payment_status"]) && strtolower($paypal->pp_data["payment_status"])=="refunded"){
        //paypal process refund email here.
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: 'Transparencia Venezuela' <noreply@" . $_SERVER['HTTP_HOST'] . "> \n";
        $subject = "Reembolso";
        $message = "Hola ".$paypal->pp_data["first_name"]." ".$paypal->pp_data["last_name"].", <br /> Sólo queríamos hacerle saber que hemos devuelto su donación.<br /><br />";
        $message .= "ID:".$paypal->pp_data["txn_id"]."<br />";
        $message .= "Monto: $".number_format($paypal->pp_data["payment_gross"],2)."<br />";
        $message .= "<br /><br />Atentamente,<br />Transparencia Venezuela";
        mail($paypal->pp_data['payer_email'], $subject, $message, $headers);
    } else {
    #**********************************************************************************************#
    #  THIS IS THE PLACE WHERE YOU WOULD INSERT ORDER TO DATABASE OR UPDATE ORDER STATUS FOR PAYPAL
    #**********************************************************************************************#
    //you can use $paypal->pp_data['XXXX'] -> where XXXX is any variable which you will see in
    //confirmation email which is sent below (you will need to do a test transaction to receive this email)

    #**********************************************************************************************#
        //creating message for sending
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: 'Transparencia Venezuela' <noreply@" . $_SERVER['HTTP_HOST'] . "> \n";
        $subject = "Nueva Donación Recibida";
        $message = "Un nueva donación se ha recibido a través de Paypal Standard<br />";
        $message .= "from " . $paypal->pp_data['payer_email'] . " on " . date('m/d/Y') . " at " . date('g:i A') . ".<br />";
        $message .= "<br />Información de PayPal:<br />";
        foreach($paypal->pp_data as $k=>$v){
            $message .= "<br /><strong>".$k."</strong>: ".$v;
        }
        mail($admin_email, $subject, $message, $headers);

        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: 'Transparencia Venezuela' <noreply@" . $_SERVER['HTTP_HOST'] . "> \n";
        $subject = "Donación recibida";
        $message = "Hola ".$paypal->pp_data["first_name"]." ".$paypal->pp_data["last_name"].", <br /> Hemos recibido correctamente una donación con paypal.<br /><br />";
        $message .= "ID:".$paypal->pp_data["txn_id"]."<br />";
        $message .= "Monto: $".number_format($paypal->pp_data["mc_gross"],2)."<br />";
        $message .= "<br /><br />Atentamente,<br />Transparencia Venezuela";
        mail($paypal->pp_data['payer_email'], $subject, $message, $headers);
    }
}

?>